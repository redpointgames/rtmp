package chunk_test

import (
	"gitlab.com/redpointgames/rtmp/chunk"
	"github.com/stretchr/testify/mock"
)

type MockStream struct {
	mock.Mock
}

func (s *MockStream) In() <-chan *chunk.Chunk {
	args := s.Called()

	return args.Get(0).(chan *chunk.Chunk)
}
