package data_test

import (
	"gitlab.com/redpointgames/rtmp/chunk"
	"gitlab.com/redpointgames/rtmp/cmd/data"
	"github.com/stretchr/testify/mock"
)

type MockParser struct {
	mock.Mock
}

var _ data.Parser = new(MockParser)

func (m *MockParser) Parse(c *chunk.Chunk) (data.Data, error) {
	args := m.Called(c)

	if args.Get(0) == nil {
		return nil, args.Error(1)
	}
	return args.Get(0).(data.Data), args.Error(1)
}
